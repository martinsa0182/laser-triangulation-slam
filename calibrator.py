from __future__ import division

import io
import time
import picamera
import operator

from PIL import Image
from math import pi, radians, sin, cos, tan, hypot

import intrinsicParameters as ip

class calibrationScanner:

	def processSlice(self,img):
			
		r,g,b = img.split()

		redData = list(r.getdata())

		iv = int(ip.pv/2)

		rowData = redData[iv*ip.pu:iv*ip.pu+ip.pu]

		iu, value = max(enumerate(rowData), key=operator.itemgetter(1)) #this could do with improvement

		if value > ip.threshold:

			R = pi/2 - (iu*ip.qu)/ip.pu + ip.qu/2
			l = (ip.a*sin(ip.L))/sin(pi - (R+ip.L))

			x = -l*cos(R)
			y = l*sin(R)

			print(iv)
			print("pixel index [%i]\nX,Y coordinates [%f,%f]\n Distance [%f]"%(iu,x,y,hypot(x,y)))
		else:
			print("Can't find laser")


		iv2 = int(ip.pv/2-ip.pv/4)

		rowData = redData[iv2*ip.pu:iv2*ip.pu+ip.pu]

		iu2, value2 = max(enumerate(rowData), key=operator.itemgetter(1)) #this could do with improvement
		if value2 > ip.threshold:
			print("laser twist: %f"%(iu-iu2))


	def captureSlices(self):
		stream = io.BytesIO()

		for i in range(10):

			raw_input("Press Enter to continue...")

			yield stream

			print("%i/360 scans complete"%i)

			stream.seek(0)
			img = Image.open(stream).rotate(90)
			self.processSlice(img)
			
			stream.seek(0)
			stream.truncate()

	def scan(self):

		#resets previous scan

		with picamera.PiCamera() as camera:
			camera.resolution = (ip.pv, ip.pu)
			camera.framerate = 30
			print("Warming up camera")
			time.sleep(2)
			print("Camera warmed up")
			camera.capture_sequence(self.captureSlices(), 'jpeg', use_video_port=True)
			print("Capture finished. Resetting turntable...")

		print("Turntable reset.")


if __name__ == "__main__":

	ts = calibrationScanner()


	ts.scan()