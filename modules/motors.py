from __future__ import division

from math import pi
import time

from motor import motor
import intrinsicParameters as ip 



class motors:

	def __init__(self,robot):
		self.robot = robot
		self.tt = motor(ip.turntablePort)
		self.lm = motor(ip.lMotorPort)
		self.rm = motor(ip.rMotorPort)

	def move(self):
		while not (self.lm.reachedTarget() and self.rm.reachedTarget() and self.tt.reachedTarget()):
			self.tt.update()
			self.lm.update()
			self.rm.update()

			time.sleep(ip.motorSpeed)

	def turn(self, angle):

		steps = angle*((pi*ip.wheelBase)/360)*(ip.motorSteps/(pi*ip.wheelDiameter))

		self.lm.rotate(steps)
		self.rm.rotate(-steps)
		self.move()
	
	def forward(self, mm):

		steps = mm*(ip.motorSteps/(pi*ip.wheelDiameter))

		self.lm.rotate(steps)
		self.rm.rotate(steps)
		self.move()

	def turntable(self, steps): #out of ip.motorSteps This is an absolute value
		
		self.tt.rotateTo(steps)
		self.move()