from __future__ import division
import io
import time

import operator
import picamera

from PIL import Image
from math import pi, radians, sin, cos, tan, hypot

from pointcloud import pointcloud
import intrinsicParameters as ip

class scanner:

	def __init__(self,robot):
		self.robot = robot

	def processSlice(self,i,img):
			
		r,g,b = img.split()

		redData = list(r.getdata())
 
		for iv in range(0,ip.pv,ip.stepSize): # should be 144 thingies

			rowData = redData[iv*ip.pu:iv*ip.pu+ip.pu]

			iu, value = max(enumerate(rowData), key=operator.itemgetter(1)) #this could do with improvement

			if value > ip.threshold:

				theta = radians((i*360)/ip.numberOfSlices) #adjust here if there are not 360 slices


				R = pi/2 - (iu*ip.qu)/ip.pu + ip.qu/2
				l = (ip.a*sin(ip.L))/sin(pi - (R+ip.L))

				x = -l*cos(R)
				y = l*sin(R)

				y = 1.234*y -11.839
				
				z = hypot(x,y)*tan(ip.qv/2 - (iv*ip.qv)/ip.pv)

				xPrime = x*cos(theta) - y*sin(theta)
				yPrime = x*sin(theta) + y*cos(theta)

				point = (xPrime,yPrime,z)
				self.scan.add(point)


	def captureSlices(self):
		stream = io.BytesIO()

		for i in range(ip.numberOfSlices):

			yield stream

			self.robot.motors.turntable((i/ip.numberOfSlices)*ip.motorSteps - ip.motorSteps/2)

			stream.seek(0)
			img = Image.open(stream)
			self.processSlice(i,img)

			#data = np.fromstring(stream.getvalue(), dtype=np.uint8)
			#print(type(data),data)
			# "Decode" the image from the array, preserving colour

			#img.save("test_%i.jpeg"%i)

			stream.seek(0)
			stream.truncate()

			print("%i/%i complete"%(i,ip.numberOfSlices))

	def scan(self):

		self.scan = pointcloud()

		self.robot.motors.turntable(-ip.motorSteps/2)

		with picamera.PiCamera() as camera:
			camera.resolution = (ip.pv, ip.pu)
			camera.framerate = 30
			camera.rotation = 90
			print("Warming up camera")
			time.sleep(2)
			print("Camera warmed up")
			camera.capture_sequence(self.captureSlices(), 'jpeg', use_video_port=True)
			print("Capture finished. Resetting turntable...")

		self.robot.motors.turntable(0)

		print("Turntable reset.")

		return self.scan