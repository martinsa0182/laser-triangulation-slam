import RPi.GPIO as GPIO

class motor:
	def __init__(self,pins):

		self.halfstep = [[1,0,0,1],
						[1,0,0,0],
						[1,1,0,0],
						[0,1,0,0],
						[0,1,1,0],
						[0,0,1,0],
						[0,0,1,1],
						[0,0,0,1]]


		self.pins = pins

		GPIO.setmode(GPIO.BOARD)
		for pin in self.pins:
			GPIO.setup(pin,GPIO.OUT)
			GPIO.output(pin, 0)

		self.reset()

	def reset(self):
		self.targetStep = 0
		self.currentStep = 0

	def rotate(self,steps):
		self.targetStep += int(steps)

	def rotateTo(self,steps):

		self.targetStep = int(steps)

	def reachedTarget(self):
		
		steps = self.targetStep - self.currentStep	
		return steps == 0

	def update(self):

		steps = self.targetStep - self.currentStep
		
		if steps == 0:
			return

		i = 1
		if steps < 0:
			i = -1

		self.currentStep += i

		newState = self.halfstep[self.currentStep%len(self.halfstep)]
		for pinIndex, status in enumerate(newState):
			GPIO.output(self.pins[pinIndex], status)