import numpy as np
import pcl
import intrinsicParameters as ip

class pointcloud:
	def __init__(self):
		print("initialising pointcloud")
		self.points = []


	def __repr__(self):
		return "Pointcloud len:%i"%len(self.points)

	def add(self,point):

		self.points.append(point)
	"""
	def loadDeadzone(self):

		print("Loading Deadzone")

		robotPosition = (ip.scanArea/2,ip.scanArea/2)

		self.load("resources/deadZone2.obj",robotPosition)
	"""
	def load(self,filepath,robotPosition):

		print("Loading file %s"%filepath)

		x,y = robotPosition

		f = open(filepath, 'r')
		fileData = f.read().split("\n")

		for pointIndex in range(len(fileData)):
			if fileData[pointIndex]:
				if fileData[pointIndex][0] == "v":
					point = fileData[pointIndex].split(" ")
					self.add((float(point[1])+x,float(point[3])+y,float(point[2])))

	def warpTo(self,target):

		print("Warping pointcloud to target")

		source = pcl.PointCloud(np.asarray(self.points))
		target = pcl.PointCloud(np.asarray(target.points))

		converged,transf,estimate,fitness = pcl.registration.icp(source,target,20)

		self.points = estimate.tolist()

	def join(self,target):
		print("Joining pointclouds")
		self.points += target.points

	def save(self,fileName):
		print("Saving pointcloud as %s"%fileName)

		objFile = open(fileName, 'w')

		objFile.write("o object\n")

		for point in self.points:
			objFile.write("v %f %f %f\n"%(point[0],point[2],point[1]))