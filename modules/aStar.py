
class aStar:

	def __init__(self,voxelGrid):
		print("Initialising route planner")
		self.voxGrid = voxelGrid

	def getPath(self,source,target):
		print("getting A star")
		openSet = []
		closedSet = []

		source.gCost = 0
		source.hCost = self.getDistance(source,target)

		openSet.append(source)

		while openSet:
			print("Iterating A star",len(openSet))
			#current = sorted(openSet, key=lambda vox: vox.fCost())[0]

			current = openSet[0]
			for vox in openSet[1:]:
				if (vox.fCost() < current.fCost) or (vox.fCost() == current.fCost()):
					if vox.hCost < current.hCost:
						current = vox

			openSet.remove(current)

			closedSet.append(current)

			if current == target:
				return self.retrace(source,target)

			for i,j in [(1,1),(1,0),(1,-1),(0,1),(0,-1),(-1,1),(-1,0),(-1,-1)]:
				neighbour = self.voxGrid.voxels[current.u+i][current.v+j]
				
				if (not neighbour.attr["safe"]) or (neighbour in closedSet):
					continue

				newCostToNeighbour = current.gCost + self.getDistance(current,neighbour)

				if (newCostToNeighbour < neighbour.gCost) or (neighbour not in openSet):
					neighbour.gCost = newCostToNeighbour
					neighbour.hCost = self.getDistance(neighbour,target)
					neighbour.parent = current

					if neighbour not in openSet:
						openSet.append(neighbour)

	def retrace(self,source,target):
		path = []
		current = target

		while current != source:
			path.append(current)
			current = current.parent

		path.reverse()

		return path

	def getDistance(self,nodeA,nodeB):

		distU = abs(nodeA.u - nodeB.u)
		distV = abs(nodeA.v - nodeB.v)
		if distU > distV:
			return 14*distV + 10*(distU-distV)
		else:
			return 14*distU + 10*(distV-distU)
