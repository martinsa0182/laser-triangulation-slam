from math import radians

#########
#Camera Parameters
#########

pu = 972 			#camera horizontal FOV in pixels
pv = 1296 			#camera vertical FOV in pixels

#tested
#qu = radians(46.69) #camera horizontal FOV in radians
#qv = radians(62.15) #camera vertical FOV in radians
#documented
#qu = radians(48.8) #camera horizontal FOV in radians
#qv = radians(62.2) #camera vertical FOV in radians

qu = radians(48.8) #camera horizontal FOV in radians
qv = radians(62.2) #camera vertical FOV in radians

#########
#Scanner Hardware Parameters
#########

a = 17.5 			#Boom Length in cm

L = radians(90-25) 	#Laser Angle in radians

#########
#Odometry Hardware Parameters
#########

wheelBase = 168.63
wheelDiameter = 62.1


#########
#Robot Hardware Parameters
#########

robotTop = 3 		#highest point of the robot. Used for calculating collision points
robotBottom = -15 	#lowest point. Used for calculating collision points

deadZone = 25 		#the area around the robot that the robot can't scan but assumes is clear

#########
#Laser Identification Parameters
#########

threshold = 30 		#Laser Identification threshold out of 255

stepSize = 8 		#Number of vertical pixel steps 

numberOfSlices = 360 #number of horisontal slices

#########
#Electronic Parameters
#########

turntablePort = [15,13,11,7]
lMotorPort = [22,18,16,12]
rMotorPort = [37,33,31,29]

motorSteps = 4104

motorSpeed = 0.001


#########
#Traversability Parameters
#########

occupancyThreshold = 0.3 	#number of vertices per cm^2
flatnessThreshold = 0.3 	#maximum height difference per cm
skewThreshold = 0.3 		#average skew distance per 1 voxel

voxelSize = 5 				#voxel size in cm
scanArea = 1000 			#total scan area in cm
