from __future__ import division

from voxel import voxel
from math import hypot

from PIL import Image


import intrinsicParameters as ip

class voxGrid:

	def __init__(self, pointcloud, robotPosition):

		print("Converting pointcloud to voxgrid...")

		self.robotPosition = robotPosition

		self.voxCount = int(ip.scanArea/ip.voxelSize)

		self.voxels = []

		self.largeNeighbourhood = [(-4, -2), (-4, -1), (-4, 0), (-4, 1), (-4, 2), (-3, -3), (-3, -2), (-3, -1), (-3, 0), (-3, 1), (-3, 2), (-3, 3), (-2, -4), (-2, -3), (-2, -2), (-2, -1), (-2, 0), (-2, 1), (-2, 2), (-2, 3), (-2, 4), (-1, -4), (-1, -3), (-1, -2), (-1, -1), (-1, 0), (-1, 1), (-1, 2), (-1, 3), (-1, 4), (0, -4), (0, -3), (0, -2), (0, -1), (0, 1), (0, 2), (0, 3), (0, 4), (1, -4), (1, -3), (1, -2), (1, -1), (1, 0), (1, 1), (1, 2), (1, 3), (1, 4), (2, -4), (2, -3), (2, -2), (2, -1), (2, 0), (2, 1), (2, 2), (2, 3), (2, 4), (3, -3), (3, -2), (3, -1), (3, 0), (3, 1), (3, 2), (3, 3), (4, -2), (4, -1), (4, 0), (4, 1), (4, 2)]
		self.smallNeighbourhood = [(0,-1),(0,1),(-1,0),(1,0)]


		for u in range(self.voxCount):
			self.voxels.append([])
			for v in range(self.voxCount):
				self.voxels[u].append(voxel(ip.voxelSize,u,v))
  
		for point in pointcloud.points:

			u = int(point[0]/ip.voxelSize)
			v = int(point[1]/ip.voxelSize)

			if (0 <= u < self.voxCount) and (0 <= v < self.voxCount):
				self.voxels[u][v].addPoint(point)

		self.update()

	def update(self):
		print("Updating voxgrid")
		for u in range(self.voxCount):
			for v in range(self.voxCount):
				self.voxels[u][v].update()


	def fillDeadzone(self):

		center = int((ip.scanArea/2)/ip.voxelSize)
		
		for u in range(center-6,center+6):
			for v in range(center-6,center+6):
				if not self.voxels[u][v].state["occupied"] or self.voxels[u][v].state["skewed"]:
					self.voxels[u][v].attr["safe"] = True


	def setShadows(self):
		print("Setting obstacle shadows")
		for u in range(self.voxCount):
			for v in range(self.voxCount):
				if self.voxels[u][v].state["obstacle"]:
					self.voxels[u][v].attr["shadow"] = True
					for i,j in self.largeNeighbourhood:
						self.voxels[u+i][v+j].attr["shadow"] = True

	def setExploration(self):
		print("Setting exploration area")
		for u in range(self.voxCount):
			for v in range(self.voxCount):

				if (self.voxels[u][v].state["scanned"]) and (not self.voxels[u][v].attr["safe"] ) and (not self.voxels[u][v].state["obstacle"]) and ((not self.voxels[u][v].state["occupied"]) or self.voxels[u][v].state["skewed"]):
					self.voxels[u][v].attr["explore"] = True

	def setSafe(self):
		print("Setting safe area")
		for u in range(self.voxCount):
			for v in range(self.voxCount):		

				if self.voxels[u][v].state["occupied"] and not self.voxels[u][v].attr["shadow"] and not self.voxels[u][v].state["skewed"]:
					self.voxels[u][v].attr["safe"] = True


	def setReachable(self): #TODO

		center = int((ip.scanArea/2)/ip.voxelSize) # Need to change to robot coordinates at some point

		self.voxels[center][center].attr["reachable"] = True

		print("Setting reachable")

		while True:
			finished = True
			print("."),
			for u in range(self.voxCount):
				for v in range(self.voxCount):

					if self.voxels[u][v].attr["reachable"]:

						for i,j in self.smallNeighbourhood:

							state = self.voxels[u+i][v+j].state
							attr = self.voxels[u+i][v+j].attr

							if attr["safe"] and not attr["reachable"]:
								self.voxels[u+i][v+j].attr["reachable"] = True
								finished = False

			if finished:
				print(" ")
				break



	def getIntrestPoints(self):
		print("Getting interest points")

		checkedVoxels = []

		clumps = []

		for u in range(self.voxCount):
			for v in range(self.voxCount):

				currentVox = self.voxels[u][v]
				self.neighbourhood = []

				if (currentVox not in checkedVoxels) and currentVox.attr["explore"]:
					
					reachableGroup = False
					for i,j in self.smallNeighbourhood:
						if self.voxels[u+i][v+j].attr["reachable"]:
							reachableGroup = True

					if reachableGroup:
						
						self.checkNeighbours(currentVox)

						clumpSize = len(self.neighbourhood)
						
						clumpU = 0
						clumpV = 0
						for voxel in self.neighbourhood:
							clumpU += voxel.u/clumpSize
							clumpV += voxel.v/clumpSize
						
						clumps.append((int(clumpU),int(clumpV),clumpSize))

						checkedVoxels += self.neighbourhood

		sortedClumps = sorted(clumps, key=lambda clump: clump[2], reverse=True)   # sort by clump size
		print("")
		return sortedClumps #returns u,v,size

	
	def checkNeighbours(self,currentVox):

		self.neighbourhood.append(currentVox)

		neighbourList = []
		for i,j in [(1,0),(0,1),(-1,0),(0,-1)]:
			neighbourVox = self.voxels[currentVox.u+i][currentVox.v+j]
			if (neighbourVox.attr["explore"]) and (neighbourVox not in self.neighbourhood):
				neighbourList.append(neighbourVox)

		for neighbour in neighbourList:
			self.checkNeighbours(neighbour)


	def save(self,fileName):
		print("Saving voxgrid as %s"%fileName)

		voxelImage = Image.new("RGB", (self.voxCount, self.voxCount), "white")

		for u in range(self.voxCount):
			for v in range(self.voxCount):	

				vox = self.voxels[u][v]

				r,g,b,o = 255,255,255,0

				if vox.state["scanned"]:
					r,g,b = 0,127,0
				if vox.attr["shadow"]:
					r,g,b = 127,0,0
				if vox.state["obstacle"]:
					r,g,b = 255,0,0
				if vox.attr["explore"]:
					r,g,b = 255,255,0

				if vox.attr["safe"]:
					r,g,b = 0,0,127

				if vox.attr["reachable"]:
					r,g,b = 0,0,255

				if vox.attr["temp"]:
					r,g,b = 255,0,255

				if (u+v)%2: #checker writer. makes things easier to count
					o += 20

				voxelImage.putpixel((u,v), (r-o,g-o,b-o))

		voxelImage.resize((self.voxCount*10,self.voxCount*10), resample=Image.NEAREST).save(fileName) #resizes it first to stop antialiasing in viewer