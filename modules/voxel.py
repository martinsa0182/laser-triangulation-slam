from __future__ import division
from math import hypot

import intrinsicParameters as ip

class voxel:
	def __init__(self,voxelSize,u,v):
		self.collisionPoints = [] # expected [(1,2,3),(1,2,3)]
		self.voxelSize = voxelSize

		self.occupancyThreshold = voxelSize**2*ip.occupancyThreshold #number of verts this states that the density must be at least 0.5ppcm^2
		self.flatnessThreshold = voxelSize*ip.flatnessThreshold #height in cm this allows the slope to rise at a 10/3 ratio
		self.skewThreshold = voxelSize*ip.skewThreshold #hypot distance in cm so this should only allow the center third skews

		self.state = {"scanned":False,"occupied":False,"obstacle":False,"skewed":False}
		self.attr = {"shadow":False,"explore":False,"safe":False,"reachable":False,"temp":False}

		self.u = u
		self.v = v

		self.gCost = 100000000
		self.hCost = 0
		self.parent = None

	def fCost(self):
		return self.gCost + self.hCost

	def __repr__(self):
		self.update()
		return "vox(%i,%i)"%(self.u,self.v)


	def addPoint(self,point):
		
		if ip.robotBottom < point[2] < ip.robotTop:
			self.state["scanned"] = True
			self.collisionPoints.append(point)

	def update(self):

		if self.collisionPoints:

			if len(self.collisionPoints) > self.occupancyThreshold:
				self.state["occupied"] = True

			if self.getSkew() > self.skewThreshold:
				self.state["skewed"] = True

			if self.getElevation() > self.flatnessThreshold:
				self.state["obstacle"] = True

	def getElevation(self):

		pointHeight = []
		for point in self.collisionPoints:
			pointHeight.append(point[2])

		return max(pointHeight) - min(pointHeight)

	def getSkew(self):

		sumX,sumY = 0,0		
		for point in self.collisionPoints:
			sumX += point[0]
			sumY += point[1]
		avgX = sumX/len(self.collisionPoints)%self.voxelSize
		avgY = sumY/len(self.collisionPoints)%self.voxelSize
		center = self.voxelSize/2
		return hypot(avgX-center,avgY-center)