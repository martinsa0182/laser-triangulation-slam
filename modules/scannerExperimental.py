from __future__ import division
import io
import time

import operator
import picamera
import picamera.array

from PIL import Image
from math import pi, radians, sin, cos, tan, hypot
import numpy as np

from pointcloud import pointcloud
import intrinsicParameters as ip

class scanner:

	def __init__(self,robot):
		self.robot = robot

	def loadObjScan(self,filepath):

		f = open(filepath, 'r')
		fileData = f.read().split("\n")[1:]

		self.pointcloud = pointcloud()

		for pointIndex in range(len(fileData)):
			if fileData[pointIndex]:
				if (not pointIndex%10000) or (len(fileData)-1 == pointIndex):
					print("Adding point %i/%i"%(pointIndex,len(fileData)-1))
				point = fileData[pointIndex].split(" ")
				self.pointcloud.add((float(point[1]),float(point[3]),float(point[2])))

		return self.pointcloud

	def processSlice(self,i,red):

		#width,height = red.shape

		redSubsampled = red[:,::ip.stepSize]


		maxIndexes = redSubsampled.argmax(axis=0)
 
		for subsampled in range(0,len(maxIndexes)):

			iu = maxIndexes[subsampled]
			value = red[subsampled,iu]
			
			iv = subsampled*ip.stepSize

			if value > ip.threshold:

				theta = radians((i*360)/ip.numberOfSlices) #adjust here if there are not 360 slices


				R = pi/2 - (iu*ip.qu)/ip.pu + ip.qu/2
				l = (ip.a*sin(ip.L))/sin(pi - (R+ip.L))

				x = -l*cos(R)
				y = l*sin(R)

				y = 1.234*y -11.839
				
				z = hypot(x,y)*tan(ip.qv/2 - (iv*ip.qv)/ip.pv)

				xPrime = x*cos(theta) - y*sin(theta)
				yPrime = x*sin(theta) + y*cos(theta)

				point = (xPrime,yPrime,z)
				self.pointcloud.add(point)


	def captureSlices(self,camera):

		stream = picamera.array.PiRGBArray(camera)

		for i in range(ip.numberOfSlices):

			yield stream

			self.robot.motors.turntable((i/ip.numberOfSlices)*ip.motorSteps - ip.motorSteps/2)

			red = stream.array[:,:,2]

			self.processSlice(i,red)

			stream.seek(0)
			stream.truncate()

			#print("%i/%i complete"%(i,ip.numberOfSlices))


	def scan(self):

		self.pointcloud = pointcloud()

		self.robot.motors.turntable(-ip.motorSteps/2)

		with picamera.PiCamera() as camera:
			camera.resolution = (ip.pv, ip.pu)
			camera.framerate = 30
			print("Warming up camera")
			time.sleep(2)
			print("Camera warmed up")
			camera.capture_sequence(self.captureSlices(camera), format='bgr', use_video_port=True)
			print("Capture finished. Resetting turntable...")

		self.robot.motors.turntable(0)

		print("Turntable reset.")

		return self.pointcloud