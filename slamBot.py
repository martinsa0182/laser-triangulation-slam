from __future__ import division
#This is the main class for the robot instance

import sys

sys.path.append('modules')

from scanner import scanner as oldScanner
from scannerExperimental import scanner as scanner
import intrinsicParameters as ip
from voxGrid import voxGrid
from motors import motors
from pointcloud import pointcloud
from aStar import aStar
import time


class robot:

	def __init__(self):

		self.motors = motors(self)
		self.scanner = scanner(self)

if __name__ == "__main__":

	rob = robot()
	try:
		world = pointcloud()
		#world.loadDeadzone()
		robotPosition = (ip.scanArea/2,ip.scanArea/2)
		world.load("resources/exampleScan.obj",robotPosition)
		vox = voxGrid(world,robotPosition)
		vox.fillDeadzone()

		vox.setShadows()
		vox.setSafe()
		vox.setExploration()
		vox.setReachable()
		#clumps = vox.getIntrestPoints()
		#u,v,size = clumps[0]

		#source = vox.voxels[int(ip.scanArea/2)][int(ip.scanArea/2)]
		#target = vox.voxels[u][v]
		for i in range(-2,2):
			for j in range(-2,2):
				vox.voxels[100+i][100+j].attr["reachable"] = False
				vox.voxels[100+i][100+j].attr["safe"] = False

		source = vox.voxels[91][107]
		target = vox.voxels[109][94]
		
		routePlanner = aStar(vox)

		path = routePlanner.getPath(source,target)

		for pathPoint in path:
			print(pathPoint)
			vox.voxels[pathPoint.u][pathPoint.v].attr["temp"] = True

		vox.save("exampleScanVox.png")

	finally:
		rob.motors.turntable(0)




"""

world.createDeadzone()

		local = rob.scanner.scan()

		world.join(local)

		voxels = voxGrid(world)

		x,y = voxels.getHeading()

"""		